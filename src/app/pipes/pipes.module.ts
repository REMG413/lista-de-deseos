import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { NgModule } from '@angular/core';
import { FiltroCompletadoPipe } from './filtro-completado.pipe';

@NgModule({
  declarations: [FiltroCompletadoPipe],
  exports: [FiltroCompletadoPipe] // Se exporta para poder utilizarlo fuera de este módulo.
  // Se borra el common module porque no se va a trabajar con ningún ngIf o ngFor, así que se puede borrar.
})
export class PipesModule { }
