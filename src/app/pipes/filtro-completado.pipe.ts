import { Pipe, PipeTransform } from '@angular/core';
import { Lista } from '../models/lista.model';

@Pipe({
  name: 'filtroCompletado',
  pure: false // Por defecto todos los pipes son puros, es decir, solo se van a ejecutar cuando la acción que dispara el ciclo de detección de cambios suceda en el mismo componente donde se encuentra el pipe. Al volverlo impuro, cada vez que se dispare el ciclo de detección de cambios, sin importar en qué componente, siempre se llamará la acción del pipe.
})
export class FiltroCompletadoPipe implements PipeTransform {

  transform( listas: Lista[], completada: boolean = true ): Lista[] {
    return listas.filter( lista => lista.terminada === completada );
  }

}
