import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule) 
    // Se agrega la carpeta en la que se guardaron todas las tabs para que el direccionamiento funcione.
    // También se cambian los archivos tab.module.ts.
  }
  // Se elimina la ruta del módulo de rutas principal y se añade como un children en el módulo de rutas propio del tab1 para no perder los tabs.
  // {
  //   path: 'agregar',
  //   loadChildren: () => import('./pages/agregar/agregar.module').then( m => m.AgregarPageModule)
  // }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
