import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { ExploreContainerComponentModule } from '../../explore-container/explore-container.module';

import { Tab2PageRoutingModule } from './tab2-routing.module';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ComponentsModule, // Al importar un módulo superior a tab1.module y tab2.module, los componentes exportados por ese módulo, pueden utilizarse en los módulos hijos previamente mencionados; de querer incluir los componentes (por ejemplo, listas.component de la carpeta components) e incluirlos en declarations de tab1.module y tab2.module, se generará un error, ya que no se puede declarar el mismo componente en dos módulos del mismo nivel a la vez.
    ExploreContainerComponentModule,
    Tab2PageRoutingModule
  ],
  declarations: [Tab2Page]
})
export class Tab2PageModule {}
