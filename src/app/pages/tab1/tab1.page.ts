import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor(public deseosServie: DeseosService,
              private router: Router,
              private alertCtrl: AlertController) {

  }

  async agregarLista() {
    // this.router.navigateByUrl('/tabs/tab1/agregar');
    
    const ALERT = await this.alertCtrl.create({
      cssClass: 'alert-red',
      header: 'Nueva lista',
      // subHeader: '',
      // message: '',
      inputs: [
        {
          name: 'titulo',
          type: 'text',
          placeholder: 'Nombre de la lista'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Crear',
          handler: (data) => {
            console.log(data);
            if ( data.titulo.length === 0 ) {
              return;
            }
            
            const LISTA_ID = this.deseosServie.crearLista( data.titulo );
            this.router.navigateByUrl(`/tabs/tab1/agregar/${ LISTA_ID }`);
          }
        }
      ]
    });
    
    ALERT.present();
  }
  
  
}
