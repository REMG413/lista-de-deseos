import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ListaItem } from 'src/app/models/lista-item.model';
import { Lista } from 'src/app/models/lista.model';
import { DeseosService } from 'src/app/services/deseos.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  lista: Lista; // Esta lista viene del mismo deseosService. Las modificaciones hechas a esta*1 afectan a la lista que se tiene en el servicio.
  nombreItem = '';

  constructor( private deseosServie: DeseosService, 
              private route: ActivatedRoute ) {
    const LISTA_ID = this.route.snapshot.paramMap.get('listaId');
    // console.log(LISTA_ID);
    
    this.lista = this.deseosServie.obtenerLista( LISTA_ID ); // *1. Modificación a la lista generada en esta página, afectando a la lista del deseosService.
    // console.log(this.lista);
  }

  ngOnInit() {
  }

  agregarItem() {
    if( this.nombreItem.length === 0 ) {
      return;
    }

    const NUEVO_ITEM = new ListaItem( this.nombreItem );
    this.lista.items.push( NUEVO_ITEM );

    this.nombreItem = '';
    this.deseosServie.guardarStorage();
  }

  cambioCheck( item: ListaItem ) {
    const PENDIENTES = this.lista.items.filter( itemData => !itemData.completado ).length;
    // console.log({ PENDIENTES });

    if( PENDIENTES === 0 ) {
      this.lista.terminadaEn = new Date();
      this.lista.terminada = true;
    }
    else {
      this.lista.terminadaEn = null;
      this.lista.terminada = false;
    }

    this.deseosServie.guardarStorage();

    console.log(this.deseosServie.listas);
  }

  borrar( i: number ) {
    // Splice es una función propia de javascript que sirve para borrar elementos y, en caso de ser necesario, agregar nuevos. El primer parámetro es el índice desde donde se quire empezar a borrar, el segundo es la cantidad de elementos a borrar.
    this.lista.items.splice( i, 1 ); 

    this.deseosServie.guardarStorage();
  }
}
