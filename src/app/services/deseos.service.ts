import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';
// Para no generar el archivo de prueba, se usa el comando --skipTests = true.
@Injectable({
  providedIn: 'root'
})
export class DeseosService {
  listas: Lista[] = [];

  constructor() {
    this.cargarStorage();

    // const LISTA1 = new Lista('Recolectar piedras del infinito');
    // const LISTA2 = new Lista('Héroes a desaparecer');

    // this.listas.push(LISTA1,LISTA2);

    // console.log(this.listas);
  }

  crearLista( titulo: string ) {
    const NUEVA_LISTA = new Lista(titulo);
    this.listas.push( NUEVA_LISTA );
    this.guardarStorage();
    
    return NUEVA_LISTA.id;
  }

  borrarLista( lista: Lista ) {
    this.listas = this.listas.filter( listaData => listaData.id !== lista.id );

    this.guardarStorage();
  }

  // editarNombreLista( lista: Lista, nuevoNombre: string ) {
  //   var editarLista = this.obtenerLista( lista.id );
  //   editarLista.titulo = nuevoNombre;
    
  //   this.borrarLista( lista );
  //   this.listas.push( editarLista );
    
  //   this.guardarStorage();
  // }

  obtenerLista( id: string | number ) {
    id = Number(id);
    return this.listas.find( ListaData => ListaData.id === id );
  }

  // Esta función almacena los datos en el Local Storage. Los datos tienen que ser convertidos a String para que esto sea posible.
  guardarStorage() {
    localStorage.setItem('data', JSON.stringify(this.listas));
  }

  // Se valida si Local Storage tiene un item con la clave "data". En caso afirmativo, se llama a dicho item y se parsea para poder mostrarlo en la aplicación.
  cargarStorage() {
    if ( localStorage.getItem('data')) {
      this.listas = JSON.parse(localStorage.getItem('data'));
    }
    else {
      this.listas = [];
    }

  }
}
